# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 16:59:01 2018

@author: nicon
"""
import math


#EJERCICIO 1

def Hazard(n):
    if n % 1 == 0: #si el input es entero, es decir, su resto al dividir por 1 es cero
        res = 0 #defino una variable con el resultado
        for i in range(1, n + 1): #barro i desde 1 hasta n (el n+1 es por la funcion range)
            res = res + ((-1)**(i+1)*2)/(2*i - 1) #expresión de la sumatoria
        return res

#EJERCICIO 2

#Defino la función auxiliar es2N1
def es2N1(x):
    n = math.log(x+1,2) #el n tal que 2^n - 1 = x
    if n >= 0 and n % 1 == 0: # si se cumple que n es mayor o igual a 0 y que es entero, es decir que su resto al dividir por 1 es cero, devuelve True
        return True
    return False

#Defino la función auxiliar esPrimo
def esPrimo(x):
    NumeroDeDivisores = 0 #arranca un contador
    for i in range(1, abs(x) + 1): #barro desde 1 hasta x
        NumeroDeDivisores = NumeroDeDivisores + int(abs(x) % i == 0) #si el número es divisible por i, suma 1 al contador
    if NumeroDeDivisores == 2: #si el total de divisores del numero es 2, el numero es primo ya que se divide por 1 y por si mismo, a excepcion de 1
        return True
    return False

def Lukaku(n):
    if n >= 1 and n % 1 == 0: #la funcion requiere que el input sea mayor o igual a 1 y que n sea un número entero, es decir, que su resto al dividir por 1 sea 0
        contador = 0 #inicializa un contador para la sumatoria
        res = 1 #inicializa el indice de la sumatoria, que comienza en 1
        while contador <= n: #mientras que el resultado de la sumatoria parcial sea menor o igual a n, va a ejecutar la acción
            contador = contador + int(es2N1(res) and esPrimo(res)) #le suma al contador 1 si son True las funciones es2N1 y esPrimo evaluadas en el índice de la sumatoria. el bool es convertido en 0 o 1 mediante la función int
            if contador == n: 
                return res #si la sumatoria llegó a n, devuelve res, que es el índice tal que la sumatoria alcanza el valor n
                #la línea 42 asegura que, al devolver res, el mismo haga true las funciones es2N1 y esPrimo, ya que solo así se le suma 1 a contador y podría llegar a cumplirse que contador == n
            else:
                res = res + 1 # si la sumatoria no llegó a n, suma 1 a res y vuelve al loop

        
        
        
        
        
        
        
        