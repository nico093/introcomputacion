# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 11:10:19 2018

@author: Nicolás Nuñez Barreto 37245899
"""

def cantAparicionesSub(l1, l2):
    cantidad = 0 #inicializo "cantidad" como variable que contará coincidencias entre listas
    for i in range(len(l1)):
        for j in range(len(l2)): 
            if l1[i] == l2[j]: #barro ambas listas con indices i y j buscando coincidencias
                cantidad = cantidad + 1 #si la hay, se le suma 1 al contador
    return cantidad #devuelve el contador final

def listaTriangular(l):
    for i in range(len(l) - 1):
        Mayor = l[i + 1] > l[i] #creo un booleano que evalua si el sucesor de cada elemento de la lista l es siempre mayor al anterior
        if Mayor == False: #si en algun momento no ocurre, intentare ver si a partir de ahi el sucesor es siempre menor
            for j in range(i, len(l) - 1):
                Menor = l[j + 1] < l[j]  #creo otro booleano que evalua dichos sucesores
                if Menor == False: 
                    return False #si eso no ocurre, la lista no es triangular, por lo que devuelve false
            if Menor == True:
                return True #si eso ocurre, la lista es triangular, por lo que devuelve true
    if Mayor == True:
        return False #si la lista siempre es creciente, es decir, Mayor siempre es True, devuelve False porque la lista tampoco es triangular