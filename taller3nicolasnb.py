# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 12:01:32 2018

@author: Nicolás Nuñez Barreto, DNI: 37245899
"""
import sys
import datetime

#abro el archivo que es la primera variable que pone el usuario en la terminal
entrada = open(str(sys.argv[1]))
#la tercera es el tamaño de la ventana
ventana = int(sys.argv[3])

#creo dos variables "inicio" y "final" que van a ir promediando una ventana de dicho tamaño
inicio = 0
final = ventana
#creo dos vectores vacíos en los que tendré los datos a analizar y el resultado
Datos = []
Resultado = []

#a "Datos" se le agregan todas las componentes de la entrada
for linea in entrada:
    Datos.append(linea.split(','))
    
#primero agrego a "Resultado" listas cuyo primer componente será la diferencia de tiempos    
for r in range(len(Datos)-ventana+1):
    t1 = datetime.datetime.strptime(Datos[r][0], '%Y-%m-%dT%H:%M:%S')
    t2 = datetime.datetime.strptime(Datos[r + ventana - 1][0], '%Y-%m-%dT%H:%M:%S')
    Resultado.append([str((t2-t1).total_seconds())]) #agrega los segundos de diferencia entre el primer y el último valor de hora de la ventana

for inicio in range(0, len(Datos)-ventana+1): #barro "inicio" desde 0 hasta la cantidad de ventanas que habrá
    for j in range(1, len(Datos[0])):  #barro las diferentes columnas
        Suma = 0 #inicializo un contador para luego promediar
        for i in range(inicio,final): #barro las filas un tamaño final-inicio que siempre será el tamaño de la ventana
            if Datos[i][j] == "NA" or Datos[i][j] == "NA\n":
                Suma = "NA"
                Resultado[inicio].append(Suma)
                break
                #si alguno de los datos es "NA" en algún momento, el for se rompe y se agrega a la lista un "NA"
            else:
                Suma = Suma + float(Datos[i][j]) #mientras que ningun valor sea NA, se van sumando los valores de la ventana
        if Suma != "NA":
            Promedio = Suma/ventana
            Resultado[inicio].append(str(round(Promedio, 2)))
            #si la suma no es NA, se divide por el tamaño de la ventana (promedio) y se agrega a la lista correspondiente dentro de Resultado
    final = final + 1 #se le suma 1 a final para que el tamaño de la ventana siempre sea el mismo.

#le doy el formato pedido al resultado creando una lista llamada ResultadoFinal
ResultadoFinal = []
for t in range(len(Resultado)):
    ResultadoFinal.append(",".join(Resultado[t])) #primero junto cada lista interna con comas

salida = open(str(sys.argv[2]), 'w') #la segunda variable dada por el usuario será el nombre del archivo de salida
print("\n".join(ResultadoFinal), file=salida) #allí se imprime la lista ResultadoFinal unida con "\n" de forma tal que cada elemento esté en una línea diferente
salida.close() #termina de imprimir en el archivo final













